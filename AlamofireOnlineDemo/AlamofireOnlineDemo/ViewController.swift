//
//  ViewController.swift
//  AlamofireOnlineDemo
//
//  Created by Michael on 2019/6/17.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit
import Alamofire

struct TestJSON : Codable {
    var userId:Int
    var id:Int
    var title:String
    var completed:Bool
}

class ViewController: UIViewController {
    let session = Alamofire.Session()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func testGET(sender: AnyObject) {
        let urlString = "https://www.apple.com"
        let response = session.request(urlString).responseString { (stringResponse) in
            if stringResponse.error != nil {
                print(stringResponse.error?.localizedDescription)
            }else {
                print(stringResponse)
            }
        }
    }
    
    @IBAction func getJSON(_ sender: Any) {
        let jsonURL = "https://jsonplaceholder.typicode.com/todos/1"
        session.request(jsonURL).responseDecodable { (dataResponse:DataResponse<TestJSON>) in
            let result:TestJSON? = dataResponse.value
            print(result)
        }
    }
    
    @IBAction func upload(_ sender: Any) {
        let image = UIImage(named:"macbook.png")
        requestWith(endUrl: "http://localhost:8080/upload", imageData: image!.pngData(), parameters: [:])
    }
    
    func requestWith(endUrl: String, imageData: Data?, parameters: [String : Any] ){
        let url = endUrl
        session.upload(multipartFormData: { (formData) in
            for (key, value) in parameters {
                formData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let data = imageData{
                formData.append(data, withName: "image", fileName: "image.png", mimeType: "image/png")
            }
        }, to: endUrl)
    }

}

