//
//  CardView.swift
//  CardPlay
//
//  Created by Michael on 2019/6/18.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit

class CardView: UIImageView {

    var frontImage:UIImage = UIImage()
    var backImage:UIImage = UIImage()
    var isLocked = false
    var isFront = false
    
    func showFront(){
        if isLocked {
            return
        }
        self.image = frontImage
        isFront = true
    }
    
    func showBack(){
        if isLocked {
            return
        }
        self.image = backImage
        isFront = false
    }
    
    func lock(){
        isLocked = true
        self.layer.borderColor = UIColor.blue.cgColor
        self.layer.borderWidth = 5.0
    }
    
    func unLock(){
        isLocked = false
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1.0

    }
    
    func exchangeImage(){
        if isFront {
            showBack()
        }else {
            showFront()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("began")
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("moved")
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("ended")
        if isLocked {
           unLock()
        }else {
            lock()
        }
    } 

}
