//
//  ViewController.swift
//  CardPlay
//
//  Created by Michael on 2019/6/18.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var cardViews: [CardView]!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepare()
    }
    
    func prepare(){
        for cardView in cardViews {
            cardView.backImage = UIImage(named: "back")!
            cardView.showBack()
        }
        
        for (index, cardView) in cardViews.enumerated() {
            cardView.frontImage = UIImage(named: "front\(index).png")!
        }
    }

    @IBAction func change(_ sender: Any) {
        for cardView in cardViews {
            cardView.exchangeImage()
        }
    }
    
    
    
}

