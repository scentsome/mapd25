//
//  CardPlayTests.swift
//  CardPlayTests
//
//  Created by Michael on 2019/6/18.
//  Copyright © 2019 Zencher. All rights reserved.
//

import XCTest
@testable import CardPlay

class CardPlayTests: XCTestCase {

    var cardView:CardView?
    override func setUp() {
        cardView = CardView()
        cardView?.backImage = UIImage(named: "back")!
        cardView?.frontImage = UIImage(named: "front0")!
    }

    override func tearDown() {
        cardView = nil
    }
    
    func testShowBack(){
        
        
        cardView?.showBack()
        
      
        XCTAssertEqual(cardView?.image, cardView?.backImage,"should be the same")
        
    }
    
    func testShowBackWhenLocked() {
        cardView?.showFront()
        cardView?.lock()
        cardView?.showBack()
        
        XCTAssertEqual(cardView?.image, cardView?.frontImage,"should be the same")
    }

    func testExample() {
       
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
