//
//  MyCollectionCell.swift
//  CollectionDemo
//
//  Created by Michael on 2019/6/24.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit

class MyCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var myLabel: UILabel!
}
