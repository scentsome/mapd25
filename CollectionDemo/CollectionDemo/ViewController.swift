//
//  ViewController.swift
//  CollectionDemo
//
//  Created by Michael on 2019/6/24.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var horizantalLine: NSLayoutConstraint!
    var data:[[String]] = []
    @IBOutlet weak var collection: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.data = [["One","Two","Three","One","Two","Three","One","Two","Three","One","Two","Three"]
        ]
        
        self.data += [["first","second","third","first","second","third"]]
        
        let flow = collection.collectionViewLayout as! UICollectionViewFlowLayout
        flow.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    override func viewWillLayoutSubviews() {
        if view.bounds.size.width > view.bounds.size.height {
            widthConstraint.constant = 600
        }else {
            widthConstraint.constant = 360
        }
    }

}

extension ViewController : UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return self.data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.data[section].count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell:MyCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "GLCell", for: indexPath) as! MyCollectionCell
        cell.myLabel.text = data[indexPath.section][indexPath.row]
        return cell
        
    }
    
    
}



