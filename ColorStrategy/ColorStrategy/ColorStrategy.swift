//
//  ColorStrategy.swift
//  ColorStrategy
//
//  Created by Michael on 2019/6/17.
//  Copyright © 2019 Zencher. All rights reserved.
//

import Foundation
import UIKit

protocol ColorStrategy {
    func execute() -> UIColor
}
