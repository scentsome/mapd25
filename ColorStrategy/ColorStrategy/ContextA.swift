//
//  ContextA.swift
//  ColorStrategy
//
//  Created by Michael on 2019/6/17.
//  Copyright © 2019 Zencher. All rights reserved.
//

import Foundation
import UIKit
class ContextA : ColorStrategy {
    func execute() -> UIColor {
        return UIColor.blue
    }
    
}
class ContextB : ColorStrategy {
    func execute() -> UIColor {
        return UIColor.gray
    }
    
}
class ContextC : ColorStrategy {
    func execute() -> UIColor {
        return UIColor.yellow
    }
    
}
