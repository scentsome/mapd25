//
//  ViewController.swift
//  ColorStrategy
//
//  Created by Michael on 2019/6/17.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var myLabel: UILabel!
    var colorStrategy:ColorStrategy?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    func selectStrategy(strategy:ColorStrategy){
        colorStrategy = strategy
    }
    @IBAction func selectA(_ sender: Any) {
        selectStrategy(strategy: ContextA())
    }
    @IBAction func selectB(_ sender: Any) {
        selectStrategy(strategy: ContextB())
    }
    @IBAction func selectC(_ sender: Any) {
        selectStrategy(strategy: ContextC())
    }
    @IBAction func change(_ sender: Any) {
        let selectedColor = colorStrategy?.execute()
        myLabel.backgroundColor = selectedColor
    }
    

}

