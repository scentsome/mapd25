//
//  ViewController.swift
//  CoreData_using_core_data
//
//  Created by Michael on 2019/6/24.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var cars: [(name:String, price:Int)] = []
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func addCar(_ sender: Any) {
        let alert = UIAlertController(title: "New Car",message: "Add a new car",preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",style: .default) { [unowned self] action in
            guard let nameField = alert.textFields?.first,
                let nameToSave = nameField.text else {
                    print("name is empty")
                    return
            }
            guard let priceField = alert.textFields?[1],
                let priceToSave = priceField.text,
                let price = Int(priceToSave) else {
                    print("price is not good")
                    return
            }
            self.cars.append((nameToSave,price))
            self.tableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addTextField()
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)

    }
    
}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellID = "CellID"
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellID)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellID)
        }
        cell?.textLabel?.text = cars[indexPath.row].name
        cell?.detailTextLabel!.text = "\(cars[indexPath.row].price)"
        return cell!
    }
}
