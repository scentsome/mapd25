//
//  ViewController.swift
//  CoreData_using_core_data
//
//  Created by Michael on 2019/6/24.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    var cars: [NSManagedObject] = []
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Car")
        NSFetchRequest<NSManagedObject>(entityName: "Car")
        let predicate = NSPredicate(format: "price > 1000")
        fetchRequest.predicate = predicate
        var sortByPrice = NSSortDescriptor(key: "price", ascending: false)
        fetchRequest.sortDescriptors = [sortByPrice]
        do {
            cars = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(NSHomeDirectory())
        // Do any additional setup after loading the view.
    }

    @IBAction func addCar(_ sender: Any) {
        let alert = UIAlertController(title: "New Car",message: "Add a new car",preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",style: .default) { [unowned self] action in
            guard let nameField = alert.textFields?.first,
                let nameToSave = nameField.text else {
                    print("name is empty")
                    return
            }
            guard let priceField = alert.textFields?[1],
                let priceToSave = priceField.text,
                let price = Int(priceToSave) else {
                    print("price is not good")
                    return
            }
//            self.cars.append((nameToSave,price))
            self.savaData(newData: (nameToSave,price))
            self.tableView.reloadData()
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default)
        alert.addTextField()
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true)

    }
    
    func savaData(newData:(name:String, price:Int)){
        guard let appDelegate =  UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        let entity =   NSEntityDescription.entity(forEntityName: "Car", in: managedContext)!
        let car = NSManagedObject(entity: entity,insertInto: managedContext)
        car.setValue(newData.name, forKeyPath: "name")
        car.setValue(newData.price, forKeyPath: "price")
        do {
            try managedContext.save()
            cars.append(car)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cars.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellID = "CellID"
        var cell:UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: cellID)
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: cellID)
        }
        let car:NSManagedObject = cars[indexPath.row]
        cell?.textLabel?.text = car.value(forKey: "name") as? String
        
        let price = car.value(forKey: "price") as! Int
        
        cell?.detailTextLabel!.text = "\(price)"
//        cell?.textLabel?.text = cars[indexPath.row].name
//        cell?.detailTextLabel!.text = "\(cars[indexPath.row].price)"
        return cell!
    }
}


extension ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") { (action, view, handler) in
            let toDeleteObject = self.cars[indexPath.row]
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            let managedContext = appDelegate.persistentContainer.viewContext
            managedContext.delete(toDeleteObject)
            do {
                try managedContext.save()
                self.cars.remove(at: indexPath.row)
                let indexSet = IndexSet([0])
                self.tableView.reloadSections(indexSet, with: UITableView.RowAnimation.fade)
                print("Deleted object")
            }catch {
                print(error)
            }

        }
        deleteAction.backgroundColor = .red
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
        return configuration
    }
}
