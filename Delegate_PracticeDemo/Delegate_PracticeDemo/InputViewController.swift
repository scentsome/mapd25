//
//  InputViewController.swift
//  Delegate_PracticeDemo
//
//  Created by Michael on 2019/6/14.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit


protocol InputViewControllerDelegate {
    func inputVCWithData(_ inputVC:InputViewController, dataString:String)
}

class InputViewController: UIViewController {
    
    @IBOutlet weak var inputField1: UITextField!
    
    
    @IBOutlet weak var inputField2: UITextField!
    var dataString:String = "data string"
    var delegate:InputViewControllerDelegate?
    
    var callBack:((String) -> Void)?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(forName: UIResponder.keyboardDidShowNotification, object: nil, queue: OperationQueue()) { (notification) in
            print(notification.userInfo!["UIKeyboardFrameEndUserInfoKey"])
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func grayHome(segue:UIStoryboardSegue){
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func done(_ sender: Any) {
        callBack?(inputField2.text!)
    }
    
}
