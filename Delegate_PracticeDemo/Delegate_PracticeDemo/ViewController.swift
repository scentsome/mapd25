//
//  ViewController.swift
//  Delegate_PracticeDemo
//
//  Created by Michael on 2019/6/14.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var label: UILabel!
    
//    func inputVCWithData(_ inputVC: InputViewController, dataString: String) {
//        self.label.text = dataString
//    }
    
//    func myClosure(str:String) -> Void {
//        label.text = str
//        dismiss(animated: true, completion: nil)
//
//    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func home(segue:UIStoryboardSegue){
        print(segue.source)
        print(segue.destination)
        
//        let inputVC:InputViewController? = segue.source as? InputViewController
//
//        if inputVC != nil {
//            inputVC?.delegate = self
//            print(inputVC!.inputField1)
//
//            let stringValue1 = inputVC!.inputField1.text!
//            let stringValue2 = inputVC!.inputField2.text!
//
//            let oInt1:Int? = Int(stringValue1)
//            let oInt2:Int? = Int(stringValue2)
//
//            if oInt1 != nil && oInt2 != nil {
//                inputVC?.delegate?.inputVCWithData(inputVC!, dataString: "\(oInt1! + oInt2!)")
//            }else {
//
//                inputVC?.delegate?.inputVCWithData(inputVC!, dataString: "Sorry Error!!")
//
//            }
//        }
        
        
        
        
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print(segue.source)
        print(segue.destination)
        
        let inputVC:InputViewController = segue.destination as! InputViewController
        print(inputVC.dataString)
        
        inputVC.callBack = { (str:String) -> Void in
            self.label.text = str
            self.dismiss(animated: true, completion: nil)
            
        }
    }
}

