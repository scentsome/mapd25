//
//  Channel.swift
//  FirebaseChatroom
//
//  Created by Michael on 2019/6/24.
//  Copyright © 2019 Zencher. All rights reserved.
//

import Foundation

struct Channel {
    var referenceID = ""
    var name = ""
}
