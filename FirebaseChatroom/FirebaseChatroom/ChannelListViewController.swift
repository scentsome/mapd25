//
//  ChannelListViewController.swift
//  FirebaseChatroom
//
//  Created by Michael on 2019/6/24.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit
import Firebase

class ChannelListViewController: UIViewController {
    
    var channelRef: DatabaseReference?
    var channelHandle: DatabaseHandle?

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var createChannelField: UITextField!
    
    var senderDisplayName = ""
    var channels = [Channel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Chatroom"
        channelRef = Database.database().reference().child("channels")
        observeChannels()
    }
    
    func observeChannels(){
        channelHandle = channelRef?.observe(DataEventType.childAdded, with: { (snapshot) in
            var channelData:[String:Any]? = snapshot.value as? [String:Any]
            let referenceKey = snapshot.key
            let chatRoomName:String? = channelData?["name"] as? String
            if chatRoomName != nil {
                let channel = Channel(referenceID: referenceKey, name: chatRoomName!)
                self.channels.append(channel)
                self.tableView.reloadData()
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func createChannel(_ sender: Any) {
    }
    
}

extension ChannelListViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channels.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "ExistingCell")
        cell?.textLabel?.text = channels[indexPath.row].name
        return cell!
    }
}


extension ChannelListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel = channels[indexPath.row]
        let chatVC = storyboard?.instantiateViewController(withIdentifier: "ChatViewController") as! ChatViewController
        print(Auth.auth().currentUser?.uid)
        chatVC.channel = channel
        chatVC.myID = Auth.auth().currentUser?.uid
        chatVC.reference = channelRef?.child(channel.referenceID)
        chatVC.displayName = senderDisplayName
        navigationController?.pushViewController(chatVC, animated: true)
    }
}
