//
//  ChatViewController.swift
//  FirebaseChatroom
//
//  Created by Michael on 2019/6/24.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit
import MessageKit
import Firebase


struct FireMessage : MessageType {
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
}

class ChatViewController: MessagesViewController {
    
    var messageRef:DatabaseReference?
    var messageHandle:DatabaseHandle?
    var channel: Channel?
    var displayName = ""
    var reference:DatabaseReference?
    var myID:String?
    var messages: [MessageType] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.messagesCollectionView.messagesDataSource = self
        self.messagesCollectionView.messagesLayoutDelegate = self
        self.messagesCollectionView.messagesDisplayDelegate = self

        self.messageInputBar.delegate = self
        observeMessage()
    }
    
    func observeMessage(){
        messageRef = reference?.child("messages")
        var messageQuery = messageRef?.queryLimited(toLast: 25)
        messageHandle = messageQuery?.observe(DataEventType.childAdded, with: { (snapshot) in
            var messageData = snapshot.value as! [String:String]
            let firMessage = FireMessage(sender: Sender(senderId: messageData["senderID"]!, displayName: messageData["senderName"]!), messageId: snapshot.key , sentDate: Date(), kind: .text(messageData["text"]!))
            self.messages.append(firMessage)
            self.messagesCollectionView.reloadData()
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChatViewController : MessagesDataSource {
    func currentSender() -> SenderType {
        return Sender(id: self.myID!, displayName: "Michael")
    }
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView:  MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
}
extension ChatViewController : MessagesLayoutDelegate {
}
extension ChatViewController : MessagesDisplayDelegate {
}

extension ChatViewController : MessageInputBarDelegate {
    func inputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        let itemRef = messageRef?.childByAutoId()
        let firMessage = FireMessage(sender: Sender(senderId: self.myID!, displayName: "Michael"), messageId: (itemRef?.key)! , sentDate: Date(), kind: .text(text))
        itemRef?.setValue(["senderID":firMessage.sender.senderId,"senderName":displayName,"text":text])
        //   messages.append(firMessage)
        inputBar.inputTextView.text = ""
        messagesCollectionView.reloadData()
        inputBar.inputTextView.resignFirstResponder()
    }
}
