//
//  ViewController.swift
//  FirebaseChatroom
//
//  Created by Michael on 2019/6/24.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {
    @IBOutlet weak var nameField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        do {
            try Auth.auth().signOut()
        }catch {
            print(error)
        }
        // Do any additional setup after loading the view.
    }

    @IBAction func goToChannel(_ sender: Any) {
        if nameField.text! != "" {
            Auth.auth().signInAnonymously { (authResult, error) in
                if error == nil {
                    print("OK",authResult)
                    self.performSegue(withIdentifier: "ToChannel", sender: nil)
                }else {
                    print("Error",error?.localizedDescription)
                }
            }
        }
    }
    
}

