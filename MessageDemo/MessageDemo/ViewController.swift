//
//  ViewController.swift
//  MessageDemo
//
//  Created by Michael on 2019/6/24.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit
import MessageKit


struct MyMessage : MessageType {
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
}

class ViewController: MessagesViewController {

    let sender = Sender(id: "1234", displayName: "Michael")
    let friend = Sender(id: "9876", displayName: "Michael")
    var messages = [MessageType]()
    override func viewDidLoad() {
        super.viewDidLoad()
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messageInputBar.delegate = self
    }


}

extension ViewController : MessagesDataSource {
    func currentSender() -> SenderType {
        return sender
    }
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
}
extension ViewController : MessagesLayoutDelegate {
}
extension ViewController : MessagesDisplayDelegate {
}

extension ViewController : MessageInputBarDelegate {
    func inputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        let fakeMessage = MyMessage(sender: friend, messageId: "1234", sentDate: Date(), kind: .text(text))
        messages.append(fakeMessage)
        messagesCollectionView.reloadData()
        messageInputBar.inputTextView.text = ""
        messageInputBar.inputTextView.resignFirstResponder()
    }
}
