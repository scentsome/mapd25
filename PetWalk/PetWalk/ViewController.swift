//
//  ViewController.swift
//  PetWalk
//
//  Created by Michael on 2019/6/17.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var pets:[(name:String,age:Int)] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func addPet(_ sender: Any) {
        let alert = UIAlertController(title: "Add Pets", message: "Name & Age", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let name = alert.textFields![0].text!
            let age = alert.textFields![1].text!
            if Int(age) != nil {
                self.pets.append((name,Int(age)!))
                self.tableView.reloadData()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        alert.addTextField()
        alert.addTextField()
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pets.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "PetCell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "PetCell")
        }
        cell?.textLabel!.text = pets[indexPath.row].0
        return cell!
    }
    
    
}

