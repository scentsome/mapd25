//
//  ViewController.swift
//  PetWalk
//
//  Created by Michael on 2019/6/17.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var managedContext:NSManagedObjectContext!

//    var pets:[(name:String,age:Int)] = []
    var pets:[Pet] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        let fetchRequest:NSFetchRequest<Pet> = Pet.fetchRequest()
        do {
            let results = try managedContext.fetch(fetchRequest)
            if results.count > 0 {
                pets = results
            }
        }catch {
            print(error)
        }

    }

    @IBAction func addPet(_ sender: Any) {
        let alert = UIAlertController(title: "Add Pets", message: "Name & Age", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            let name = alert.textFields![0].text!
            let age = alert.textFields![1].text!
            if Int(age) != nil {
//                self.pets.append((name,Int(age)!))
                let newPet = Pet(context: self.managedContext)
                newPet.name = name
                newPet.age = Int16(age)!
                do {
                    try self.managedContext.save()
                }catch {
                    print(error)
                }
                self.pets.append(newPet)

                self.tableView.reloadData()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        
        alert.addTextField()
        alert.addTextField()
        self.present(alert, animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var walkViewController = segue.destination as! WalkViewController
        
        walkViewController.managedContext = self.managedContext
        let indexPath = tableView.indexPathForSelectedRow
        walkViewController.pet = pets[(indexPath?.row)!]
    }
    
}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pets.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "PetCell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "PetCell")
        }
//        cell?.textLabel!.text = pets[indexPath.row].0
        cell?.textLabel!.text = pets[indexPath.row].name

        return cell!
    }
    

    
}

extension ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ToWalkVC", sender: nil)
    }
}

