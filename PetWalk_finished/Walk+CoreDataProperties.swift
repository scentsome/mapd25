//
//  Walk+CoreDataProperties.swift
//  PetWalk
//
//  Created by Michael on 2019/6/28.
//  Copyright © 2019 Zencher. All rights reserved.
//
//

import Foundation
import CoreData


extension Walk {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Walk> {
        return NSFetchRequest<Walk>(entityName: "Walk")
    }

    @NSManaged public var timeStamp: NSDate?
    @NSManaged public var pet: Pet?

}
