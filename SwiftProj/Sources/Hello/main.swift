import Vapor

struct Person: Content {
    var name: String
    var age: Int
}

struct ImageData: Content {
    var image: Data
}

struct User: Content {
    var name: String
    var email: String
}

let app = try Application()
let router = try app.make(Router.self)

router.get("hello") { req in
    return "Hello, world."
}

router.post("hello") { (request) -> String in
    return "Helll POST"
}


// /filter?name=michael&age=45
router.get("filter"){ req -> String in
    
    let name = try req.query.get(String.self, at: "name")
    let age = try req.query.get(String.self, at: "age")
    
    if name != "" {
        return "Ok"+" "+name+" : "+age
    } else {
        return "No"
    }
}

// /person?name=michael&age=45
router.get("person"){ req -> String in
    
    let person = try req.query.decode(Person.self)

    if person != nil  {
        return "Ok" + " " + person.name + " : " + "\(person.age)"
    } else {
        return "No"
    }
}


router.post(User.self, at: "userInfo") { (request, data) -> User in
    
    print(data.name)
    print(data.email)
    return data
}


router.post(ImageData.self, at:"upload") { req, data -> String in
    
    var imageData:Data? = data.image
    
    if imageData != nil {
        let dest = URL(fileURLWithPath:"/Users/michael/Desktop/tmp/Test.png")
        try imageData?.write(to: dest)
        print("have image data")
        return "OK"
    }else {
        print("no image data")
        return "NO"
    }
}


try app.run()





print("Hello Swift Package Manager")
