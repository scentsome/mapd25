import Vapor
import FluentPostgreSQL

public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {
    try services.register(FluentPostgreSQLProvider())
    config.prefer(DictionaryKeyedCache.self, for: KeyedCache.self)
    
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    services.register(middlewares)
    
    var databases = DatabasesConfig()
    let databaseConfig = PostgreSQLDatabaseConfig(
        hostname: "localhost",
        username: "hello",
        database: "hello",
        password: "password2")
    let database = PostgreSQLDatabase(config: databaseConfig)
    databases.add(database: database, as: .psql)
    services.register(databases)
    
    var migrations = MigrationConfig()
    migrations.add(model: Person.self, database: .psql) // Add

    services.register(migrations)

}

final class Person: Content {
    var id:UUID?
    var name: String
    var age: Int
    
    init(name: String, age: Int) {
        self.name = name
        self.age = age
    }
}

extension Person: PostgreSQLUUIDModel {}
extension Person: Migration {}
extension Person: Parameter {}

struct ImageData: Content {
    var image: Data
}

struct User: Content {
    var name: String
    var email: String
}

var config = Config.default()
var env = try Environment.detect()
var services = Services.default()
try configure(&config, &env, &services)
let app = try Application(config: config, environment: env, services: services)


let router = try app.make(Router.self)

router.get("hello") { req in
    return "Hello, world."
}

router.post("hello") { (request) -> String in
    return "Helll POST"
}


// /filter?name=michael&age=45
router.get("filter"){ req -> String in
    
    let name = try req.query.get(String.self, at: "name")
    let age = try req.query.get(String.self, at: "age")
    
    if name != "" {
        return "Ok"+" "+name+" : "+age
    } else {
        return "No"
    }
}




router.post(User.self, at: "userInfo") { (request, data) -> User in
    
    print(data.name)
    print(data.email)
    return data
}


router.post(ImageData.self, at:"upload") { req, data -> String in
    
    var imageData:Data? = data.image
    
    if imageData != nil {
        let dest = URL(fileURLWithPath:"/Users/michael/Desktop/tmp/Test.png")
        try imageData?.write(to: dest)
        print("have image data")
        return "OK"
    }else {
        print("no image data")
        return "NO"
    }
}

router.post(Person.self, at: "addPerson") { req, data -> Future<Person> in
    
    return data.save(on: req)
}



router.get("getPeople") { req -> Future<[Person]> in
    
    return Person.query(on: req).all()
}

router.get("getPerson", Person.parameter) { req -> Future<Person> in
    
    return try req.parameters.next(Person.self)
}

router.put("updatePerson", Person.parameter) { req -> Future<Person> in
    
    return try flatMap(to: Person.self,
                       req.parameters.next(Person.self),
                       req.content.decode(Person.self)) { person, updatedPerson in
                        
                        person.name = updatedPerson.name
                        person.age = updatedPerson.age
                        return person.save(on: req)
    }
}
try app.run()






