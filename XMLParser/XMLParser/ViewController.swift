//
//  ViewController.swift
//  XMLParser
//
//  Created by Michael on 2019/6/18.
//  Copyright © 2019 Zencher. All rights reserved.
//

import UIKit
import Kanna

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        parseHtml()
        parseXML()
    }
    
    func parseHtml() {
        let html = """
        <html><h1><a href=\"https://www.apple.com\"> Title1 </a></h1>
        <h1><a href=\"https://www.apple.com\"> Title2 </a></h1>
        <h1><a href=\"https://www.apple.com\"> Title3 </a></h1>
        </html>
        """
        
        
        if let doc = try? HTML(html: html, encoding: .utf8) {
            print(doc.title)
            
            // Search for nodes by CSS
//            for link in doc.css("a, link") {
//                print(link.text)
//                print(link["href"])
//            }
            // Search for nodes by XPath
            for link in doc.xpath("//h1 ") {
                print(link.text)
                print(link["href"])
            }
        }
        
        
    }
    
    func parseXML(){
        let xml = """
        <?xml version="1.0" encoding="UTF-8" ?>
        <rss version=\"2.0\">
        <channel>
            <item>
                <title>W3Schools Home Page</title>
                <link>http://www.w3schools.com</link>
                <description>Free web building tutorials</description>
            </item>
            <item>
                <title>Apple Home Page</title>
                <link>http://www.apple.com</link>
                <description>Apple web building tutorials</description>
            </item>
        </channel>
        </rss>
        """
        if let doc = try? Kanna.XML(xml: xml, encoding: .utf8) {
//            if let titile = doc.at_xpath("//rss/channel/item/title", namespaces: nil) {
//                print(titile.text)
//            }
            for title in doc.xpath("//rss/channel/item/title") {
                print(title.text)
            }
            
            
        }
    }


}

