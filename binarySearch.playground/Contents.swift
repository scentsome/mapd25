import UIKit



func binarySearch(list:[Int], item:Int) -> Int? {
    var low = 0
    var high = list.count - 1
    while low <= high {
        let mid = (low+high) / 2
        let guess = list[mid]
        
        if guess == item {
            return mid
        }else if guess > item {
            high = mid - 1
        }else {
            low = mid + 1
        }
    }
    return nil
}

binarySearch(list: [1, 2, 3, 4, 5, 9 , 10, 15, 23, 45, 67, 100, ], item: 15)
