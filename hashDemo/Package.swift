// swift-tools-version:4.0
import PackageDescription
let package = Package(
    name: "HashDemo",
    products: [
        .executable(name: "HashDemo", targets: ["HashDemo"]),
    ],
    dependencies: [
        .package(url: "https://github.com/krzyzanowskim/CryptoSwift.git", .exact("1.0.0")),
    ],
    targets: [
        .target(
            name: "HashDemo",
            dependencies: ["CryptoSwift"]),
    ]
)
