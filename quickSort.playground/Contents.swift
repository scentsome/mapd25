import UIKit

func sum(list : [Int]) -> Int {
    if list.count == 0 {
        return 0
    }
    var tempArray = list
    tempArray.remove(at: 0)
    return list[0] + sum(list: tempArray)
}

print(sum(list: [1, 2, 3, 4, 5, 10]))

func quickSort(array:[Int]) -> [Int] {
    if array.count < 2 {
        return array
    }else {
        var temp = array
        let pivot = temp[0]
        temp.remove(at: 0)
        let lessArray = temp.filter { (ele) -> Bool in
            if ele <= pivot {
                return true
            }
            return false
        }
        let greaterArray = temp.filter { (ele) -> Bool in
            if ele > pivot {
                return true
            }
            return false
        }
        return quickSort(array:lessArray) + [pivot] + quickSort(array:greaterArray)
    }
}
var r = quickSort(array: [10,9,3,2,1,4,7,8,3,3])
r
