import UIKit

// recursion

func countDown(n:Int) {
    print(n)
    if n <= 1 {
        return
    }else {
        countDown(n: n-1)
    }
}

//countDown(n: 3)

func incremental(n:Int) {
    if n <= 0 {
        return
    }else {
        incremental(n: n-1)
    }
    print(n)
}

incremental(n: 5) // print 1, 2, 3, 4, 5
